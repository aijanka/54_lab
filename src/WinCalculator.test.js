import WinCalculator, {OUTCOMES} from './WinCalculator';


const royalFlush = [
    {suit: 'H', rank: 'Q'},
    {suit: 'H', rank: 'K'},
    {suit: 'H', rank: '10'},
    {suit: 'H', rank: 'J'},
    {suit: 'H', rank: 'A'},
];
it('should determine the Royal Flush', () => {
    const calc = new WinCalculator(royalFlush);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.ROYAL_FLUSH);
});




const flush = [
    {suit: 'H', rank: 'Q'},
    {suit: 'H', rank: 'K'},
    {suit: 'H', rank: '7'},
    {suit: 'H', rank: 'J'},
    {suit: 'H', rank: 'A'},
];
it('should determine Flush', () => {
    const calc = new WinCalculator(flush);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.FLUSH);
});



const pair =[
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: 'K'},
    {suit: 'S', rank: '7'},
    {suit: 'D', rank: '2'},
    {suit: 'H', rank: 'A'},
];
it('should determine Pair', () => {
    const calc = new WinCalculator(pair);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.PAIR);
});




const twoPairs =[
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: 'K'},
    {suit: 'S', rank: '7'},
    {suit: 'D', rank: '2'},
    {suit: 'H', rank: 'K'},
];
it('should determine Two Pairs', () => {
    const calc = new WinCalculator(twoPairs);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.TWO_PAIRS);
});



const straight = [
    {suit: 'D', rank: '2'},
    {suit: 'H', rank: '5'},
    {suit: 'C', rank: '4'},
    {suit: 'S', rank: '3'},
    {suit: 'H', rank: 'A'},
];
it('should determine Straight', () => {
    const calc = new WinCalculator(straight);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.STRAIGHT);
});




const straightFlush = [
    {suit: 'D', rank: '2'},
    {suit: 'D', rank: '5'},
    {suit: 'D', rank: '4'},
    {suit: 'D', rank: '3'},
    {suit: 'D', rank: 'A'},
];
it('should determine Straight Flush', () => {
    const calc = new WinCalculator(straightFlush);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.STRAIGHT_FLUSH);
});




const three = [
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: 'K'},
    {suit: 'S', rank: '7'},
    {suit: 'D', rank: '2'},
    {suit: 'H', rank: '2'},
];
it('should determine Three of a kind', () => {
    const calc = new WinCalculator(three);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.THREE);
});




const four = [
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: 'K'},
    {suit: 'S', rank: '2'},
    {suit: 'D', rank: '2'},
    {suit: 'H', rank: '2'},
];
it('should determine Four of a kind', () => {
    const calc = new WinCalculator(four);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.FOUR);
});





const fullHouse = [
    {suit: 'H', rank: '2'},
    {suit: 'C', rank: 'K'},
    {suit: 'S', rank: 'K'},
    {suit: 'D', rank: '2'},
    {suit: 'H', rank: '2'},
];
it('should determine Full House', () => {
    const calc = new WinCalculator(fullHouse);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.FULL_HOUSE);
});




const noComb =[
    {suit: 'H', rank: '3'},
    {suit: 'C', rank: 'K'},
    {suit: 'S', rank: '7'},
    {suit: 'D', rank: '2'},
    {suit: 'H', rank: 'A'},
];
it('should determine Nothing', () => {
    const calc = new WinCalculator(noComb);
    const result = calc.getOutcome();

    expect(result).toEqual(OUTCOMES.NOTHING);
});
