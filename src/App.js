import React, { Component } from 'react';
import './App.css';
import Card from './Card';
import WinCalculator from './WinCalculator';

class App extends Component {
    state = {
        cards: [],
        outcome: ''
    };

    shuffle = () => {
        const suits = ['D', 'H', 'C', 'S'];
        const ranks = [ '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'J', 'Q', 'K', 'A'];
        let cards = [];
        let i = 0;
        while(i < 5){
            let suit = suits[Math.floor(Math.random() * 4)];
            let rank = ranks[Math.floor(Math.random() * 14)];
            if(cards.findIndex((card) => card.suit === suit && card.rank === rank) === -1){
                const card = {suit, rank, i};
                cards.push(card);
                i++;
            }
        }
        console.log(cards);

        const calculator = new WinCalculator(cards);
        const outcome = calculator.getOutcome();
        this.setState({cards, outcome});

    };

    render(){
        // console.log(this.state.cards, 'CARDS')
        return (
            <div className="App ">
                <button onClick={this.shuffle}>Shuffle</button>
                <div className="playingCards faceImages">
                    {this.state.cards.map((card) => <Card suit={card.suit} rank={card.rank} key={card.i}/>)}

                </div>

                <div>{this.state.outcome}</div>
            </div>
        );
    }
}

export default App;
