export const OUTCOMES = {
    ROYAL_FLUSH: "Royal Flush",
    FLUSH: "Flush",
    STRAIGHT_FLUSH: 'Straight Flush',
    PAIR: 'Pair',
    TWO_PAIRS: 'Two pairs',
    FULL_HOUSE: 'Full house',
    THREE: 'Tree of a kind',
    STRAIGHT: 'Straight',
    FOUR: 'Four of a kind',
    NOTHING: 'No combinations'
};

class WinCalculator {
    constructor(cards){
        this.cards = cards;

        this.suits = this.cards.map(cards => cards.suit);
        this.ranks = this.cards.map(cards => cards.rank);
        console.log(this.ranks);
        this.isFlush = this.suits.every(suit => suit === this.suits[0]);

    }

    isRoyalFlush() {
        return this.isFlush &&
            this.ranks.includes('10') &&
            this.ranks.includes("J") &&
            this.ranks.includes('Q') &&
            this.ranks.includes('K') &&
            this.ranks.includes('A')
    };

    numOfRepeat(){
        const ranksObject = {};
        this.ranks.forEach(rank => {
            if(!ranksObject[rank]){
                ranksObject[rank] = 1;
            } else {
                ranksObject[rank]++;
            }
        })
        return Object.values(ranksObject);
    }

    numOfPairs() {
        return this.numOfRepeat().filter(num => num === 2).length;
    };

    isThree() {
        return this.numOfRepeat().includes(3);
    };

    isFour(){
        return this.numOfRepeat().includes(4);
    }

    makeCardsInt(){
        return this.ranks.map(r => {
            if(r === 'J'){
                return 11 ;
            }else if(r === 'Q'){
                return 12;
            } else if(r === 'K'){
                return 13;
            } else if(r === 'A'){
                return 14;
            } else {
                return parseInt(r,10);
            }
        });
    }

    isStraight(){
        this.ranks = this.makeCardsInt();
        this.ranks.sort((a, b) =>  a - b);
        console.log(this.ranks);
        if ((this.ranks[0] + 4) === (this.ranks[1] + 3) &&
            (this.ranks[1] + 3) === (this.ranks[2] + 2) &&
            this.ranks[2] + 2 === this.ranks[3] + 1) {
            return (this.ranks[3] + 1 === this.ranks[4] || (this.ranks[4] === 14 && Math.min(...this.ranks) === 2))
        }
    };

    getOutcome() {
        if(this.isRoyalFlush()){
            return OUTCOMES.ROYAL_FLUSH;
        } else if(this.isFlush && this.isStraight()){
            return OUTCOMES.STRAIGHT_FLUSH;
        } else if(this.isFlush) {
            return OUTCOMES.FLUSH;
        }  else if(this.numOfPairs() === 2){
            return OUTCOMES.TWO_PAIRS;
        } else if(this.numOfPairs() === 1 && this.isThree()){
            return OUTCOMES.FULL_HOUSE;
        } else if(this.numOfPairs() === 1){
            return OUTCOMES.PAIR;
        } else if(this.isThree()){
            return OUTCOMES.THREE;
        } else if(this.isFour()){
            return OUTCOMES.FOUR;
        } else if(this.isStraight()) {
            return OUTCOMES.STRAIGHT;
        } else {
            return OUTCOMES.NOTHING;
        }
    }
}

export default WinCalculator;