import React from 'react';

const suits = {
    D: {className: 'diams', symbol: "♦"},
    H: {className: 'hearts', symbol: "♥"},
    C: {className: 'clubs', symbol: '♣'},
    S: {className: 'spades', symbol: '♠'}
};

const Card = props => {
    const suitClass = suits[props.suit].className;
    const symbol = suits[props.suit].symbol;
    const cardClasses =  `card rank-${props.rank.toLowerCase()} ${suitClass}`;

    return (
        <div className={cardClasses}>
            <span className="rank">{props.rank.toUpperCase()}</span>
            <span className="suit">{symbol}</span>
        </div>
)};

export default Card;

{/*<div className={`card rank-${parseInt(props.rank, 10) ? props.rank : props.rank.toLowerCase()} ${getSuit(props.suit)}`}>*/}
